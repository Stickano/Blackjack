﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoapBlackJack
{
    public class Table
    {
        private List<Player> players;
        private CardDeck deckOne;
        private CardDeck deckTwo;

        private const int DECK_SIZE = 56;
        private byte currentDeck;

        public Table()
        {
            players = new List<Player>();
            deckOne = new CardDeck();
            deckTwo = new CardDeck();
            currentDeck = 1;
            StartTable();
        }

        /*
         * Will start a new table (shuffle the two decks)
         */
        public void StartTable()
        {
            deckOne.ShuffleDeck();
            deckTwo.ShuffleDeck();
        }

        /*
         * Allows players to the table - max 4 players, else Exception(string)
         */
        public void AddPlayer(string name)
        {
            if(players.Count < 4)
                players.Add(new Player(name));
            else 
                throw new Exception("Table Full");
        }

        /*
         * Removes a player from the table
         */
        public void RemovePlayer(Player player)
        {
            players.Remove(player);
        }

        /*
         * Sets which deck is in use
         * (The table will switch between two decks when one is used up)
         */
        private void GetDeck()
        {
            if (deckOne.UsedCards() == DECK_SIZE)
            {
                currentDeck = 2;
                deckOne.ShuffleDeck();
            }
            
            if(deckTwo.UsedCards() == DECK_SIZE)
            {
                currentDeck = 1;
                deckTwo.ShuffleDeck();
            }
        }

        /*
         * Return a card from one of the two decks
         */
        public Card GetCard()
        {
            GetDeck();
            if (currentDeck == 1)
                return deckOne.GetCard();
            else
                return deckTwo.GetCard();
        }
    }
}