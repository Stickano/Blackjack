﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoapBlackJack
{
    public class CardDeck
    {
        private const int MAX_VALUE = 14;
        private List<Card> availCards;
        private List<Card> usedCards;
        Random rand;

        public CardDeck()
        {
            availCards = new List<Card>();
            usedCards = new List<Card>();
            rand = new Random();
            CreateDeck();
            ShuffleDeck();
        }

        /*
         *  Creates a full deck of cards (56 cards)
         */
        private void CreateDeck()
        {
            string[] faces = new string[] { "Spade", "Heart", "Diamond", "Club" };
            foreach (string face in faces)
            {
                for (int i = 1; i <= MAX_VALUE; i++)
                {
                    availCards.Add(new Card(face, i));
                }
            }
        }

        /*
         * Places the cards at a random position in the deck
         */
        public void ShuffleDeck()
        {
            for (int i = 0; i < availCards.Count; i++)
            {
                int randPos = rand.Next(0, 57);
                Card temp = availCards[i];
                availCards[i] = availCards[randPos];
                availCards[randPos] = temp;
                usedCards = null;
            }
        }

        /*
         * Will return the top card in the avail bundle, and place it in used
         * along with removing it from avail.
         */
        public Card GetCard()
        {
            usedCards.Add(availCards.Last());
            availCards.Remove(availCards.Last());

            return usedCards.Last();
        }

        /*
         * Will return how many cards has been used,
         * so we can switch between deck 1 and 2 at our table.
         */
        public int UsedCards()
        {
            return usedCards.Count;
        }
    }
}