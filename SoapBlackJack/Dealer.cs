﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoapBlackJack
{
    public class Dealer : IPlay
    {
        private const int MIN_VAL = 17;
        private List<Card> cards;

        public Dealer()
        {
            cards = new List<Card>();
        }

        /*
         * This will be the first hand a player receives (one visible card)
         */
        public void StartGame(Card first, Card second)
        {
            cards.Add(first);
            cards.Add(second);
        }

        /*
         * If a player takes another card
         */
        public void GetNext(Card card)
        {
            cards.Add(card);
        }

        /*
         * Return the full card value the player has
         */
        public int GetValue()
        {
            int val = 0;
            foreach (Card card in cards)
            {
                val += card.CardValue;
            }

            return val;
        }
    }

}