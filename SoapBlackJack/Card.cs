﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoapBlackJack
{


    public class Card
    {
        public string CardFace { get; private set; }
        public int CardValue { get; private set; }

        public Card(string face, int value)
        {
            CardFace = face;
            CardValue = value;
        }

        /*
         * Returns a human readable card string (face and value)
         */
        public string GetCardString()
        {
            string val = CardValue.ToString();
            if (CardValue == 11)
                val = "Jack";
            else if (CardValue == 12)
                val = "Queen";
            else if (CardValue == 13)
                val = "King";
            else if (CardValue == 14)
                val = "Ace";

            return val + " of " + CardFace;
        }
    }
}