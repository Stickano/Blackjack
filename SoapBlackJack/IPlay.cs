﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoapBlackJack
{
    interface IPlay
    {
        void StartGame(Card first, Card second);
        void GetNext(Card card);
        int GetValue();
    }
}
